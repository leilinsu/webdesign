const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const nodemailer = require('nodemailer');
const multer = require('multer');

const PORT = 3000;

const app = express();

var filename = '';

app.use(bodyParser.json());

app.use(cors({origin: "*"}));

app.get('/', function(req, res) {
    res.send("hello from server");
});

app.post('/sendemail', function(req, res){
    console.log(req.body);
    let user = req.body;
    sendEmail(user, info => {
        res.send(info);
    });
});

app.post('/sendresume', function(req, res){
    console.log(req.body);
    let user = req.body;
    sendResume(user, info => {
        res.send(info);
    });
});

app.listen(PORT, function() {
    console.log("server running on localhost: " + PORT);
});

async function sendEmail(user, callback) {
    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        secure: false,
        auth: {
            user: 'gnrncustomerservice@gmail.com',
            pass: 'gnrn2019+1s.'
        }
    });

    let emailContent = {
        from: 'Useless field when using gmail',
        to: 'gnrncustomerservice@gmail.com',
        subject: `${user.subject}`,
        html: `<p>From: ${user.firstName} ${user.lastName}<br>Reply: ${user.email}<br>
        ${user.message}</p>`
    };

    let info = await transporter.sendMail(emailContent);

    callback(info);
}

//upload resume
/*
const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, 'uploads')
    },
    filename: (req, file, callback) => {
        callback(null, `${file.originalname}`)
    }
});

var upload = multer({storage: storage});
*/

var upload = multer({dest: 'uploads/'});

app.post('/file', upload.single('file'), (req, res, next) => {
    const file = req.file;
    filename = file.filename;

    if(!file) {
        const error = new Error('Please upload your resume.');
        error.httpStatusCode = 400;
        return next(error);
    }
    res.send(file);
});

async function sendResume(user, callback) {
    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        secure: false,
        auth: {
            user: 'gnrncustomerservice@gmail.com',
            pass: 'gnrn2019+1s.'
        }
    });

    let emailContent = {
        from: 'Useless field when using gmail',
        to: 'gnrncustomerservice@gmail.com',
        subject: `Applying for ${user.job}`,
        html: `<p>Applier Info<br> Name: ${user.firstName} ${user.lastName}<br>Email: ${user.email}<br>
        Phone: ${user.phone}</p>`,
        attachments: [
            {
                filename: filename,
                path: __dirname+'/uploads/'+filename
            }
        ]
    };

    let info = await transporter.sendMail(emailContent);

    callback(info);
}
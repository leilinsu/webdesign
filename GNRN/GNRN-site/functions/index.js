const functions = require('firebase-functions');
const admin = require('firebase-admin');
const nodemailer = require('nodemailer');

admin.initializeApp();

exports.sendEmailNotification = functions.firestore.document('submissions/{docId}')
.onCreate((snap, context)=>{
    const data = snap.data();
    let authData = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'gnrncustomerservice@gmail.com',
            pass: 'gnrn2019+1s.'
        }
    });
authData.sendMail({
    from: `${data.email}`,
    to: 'gnrncustomerservice@gmail.com',
    subject: `${data.subject}`,
    html: `<p>From: ${data.firstName} ${data.lastName}<br>Reply: ${data.email}<br>
    ${data.message}</p>`
}).then(res=>console.log('successfully sent')).catch(err=>console.log(err));
});

exports.sendResume = functions.firestore.document('resumes/{docId}')
.onCreate((snap, context)=>{
    const data = snap.data();
    let authData = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'gnrncustomerservice@gmail.com',
            pass: 'gnrn2019+1s.'
        }
    });
authData.sendMail({
    from: `${data.email}`,
    to: 'gnrncustomerservice@gmail.com',
    subject: `${data.subject}`,
    html: `<p>From: ${data.firstName} ${data.lastName} ${data.phone}<br>Reply: ${data.email}<br>
    ${data.url}</p>`
}).then(res=>console.log('successfully sent resume')).catch(err=>console.log(err));
});


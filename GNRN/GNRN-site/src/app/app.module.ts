import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule, routingComponents} from './app-routing.module';
import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductUnderboneComponent } from './product-underbone/product-underbone.component';
import { ProductScooterMotorcycleComponent } from './product-scooter-motorcycle/product-scooter-motorcycle.component';
import { ProductMotorcycleComponent } from './product-motorcycle/product-motorcycle.component';
import { ProductAtvComponent } from './product-atv/product-atv.component';
import { ProductAutomobilePartsComponent } from './product-automobile-parts/product-automobile-parts.component';
import { ProductFunctionalPartsComponent } from './product-functional-parts/product-functional-parts.component';
import { ProductExteriorTrimPartsComponent } from './product-exterior-trim-parts/product-exterior-trim-parts.component';
import { ProductService } from './product.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ProductAllComponent } from './product-all/product-all.component';
import { CategoryPipe } from './category.pipe';
import { JobDetailsComponent } from './job-details/job-details.component';
import { ApplyComponent } from './apply/apply.component';
import { ApplyService } from './apply.service';
import { TechnologiesUsedComponent } from './technologies-used/technologies-used.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestore, AngularFirestoreModule} from '@angular/fire/firestore';
import { AngularFireStorageModule} from '@angular/fire/storage';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    routingComponents,
    PageNotFoundComponent,
    ProductUnderboneComponent,
    ProductScooterMotorcycleComponent,
    ProductMotorcycleComponent,
    ProductAtvComponent,
    ProductAutomobilePartsComponent,
    ProductFunctionalPartsComponent,
    ProductExteriorTrimPartsComponent,
    ProductAllComponent,
    CategoryPipe,
    JobDetailsComponent,
    ApplyComponent,
    TechnologiesUsedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyDV7P3YCE3q8GgRBAf4sWz2kI-OpAODPqs",
      authDomain: "gnrn-62e36.firebaseapp.com",
      databaseURL: "https://gnrn-62e36.firebaseio.com",
      projectId: "gnrn-62e36",
      storageBucket: "gnrn-62e36.appspot.com",
      messagingSenderId: "190147745502",
      appId: "1:190147745502:web:f00eae919830e79d2f544f",
      measurementId: "G-TY6G90JDDT"
    }),
    AngularFireStorageModule,
    AngularFirestoreModule
  ],
  providers: [ProductService, ApplyService, AngularFirestore], //register with injector
  bootstrap: [AppComponent]
})
export class AppModule { }

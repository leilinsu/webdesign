import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-all',
  templateUrl: './product-all.component.html',
  styleUrls: ['./product-all.component.css']
})
export class ProductAllComponent implements OnInit {

  public products = [];

  constructor(private _productService: ProductService) { }

  //subscribe observable to receive data, store as local variable
  ngOnInit() {
    this._productService.getProducts()
      .subscribe(data => this.products = data);
  }

}

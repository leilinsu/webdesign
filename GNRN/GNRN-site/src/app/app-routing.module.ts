import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsListComponent } from './news-list/news-list.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { TechnologyListComponent } from './technology-list/technology-list.component';
import { QualityListComponent } from './quality-list/quality-list.component';
import { CareerListComponent } from './career-list/career-list.component';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductUnderboneComponent } from './product-underbone/product-underbone.component';
import { ProductScooterMotorcycleComponent } from './product-scooter-motorcycle/product-scooter-motorcycle.component';
import { ProductMotorcycleComponent } from './product-motorcycle/product-motorcycle.component';
import { ProductAtvComponent } from './product-atv/product-atv.component';
import { ProductAutomobilePartsComponent } from './product-automobile-parts/product-automobile-parts.component';
import { ProductFunctionalPartsComponent } from './product-functional-parts/product-functional-parts.component';
import { ProductExteriorTrimPartsComponent } from './product-exterior-trim-parts/product-exterior-trim-parts.component';
import { ProductAllComponent } from './product-all/product-all.component';
import { JobDetailsComponent } from './job-details/job-details.component';
import { ApplyComponent } from './apply/apply.component';
import { TechnologiesUsedComponent } from './technologies-used/technologies-used.component';


const routes: Routes = [
  { path: '', component: HomePageComponent},
  { path: 'news', component: NewsListComponent},
  { path: 'products', redirectTo: '/products/all', pathMatch: 'full'},
  { 
    path: 'products', component: ProductsListComponent, 
    children: [ 
      { path: 'underbone', component: ProductUnderboneComponent },
      { path: 'scooter-motorcycle', component: ProductScooterMotorcycleComponent},
      { path: 'motorcycle', component: ProductMotorcycleComponent},
      { path: 'atv', component: ProductAtvComponent},
      { path: 'automobile-parts', component: ProductAutomobilePartsComponent},
      { path: 'functional-parts', component: ProductFunctionalPartsComponent},
      { path: 'exterior-trim-parts', component: ProductExteriorTrimPartsComponent},
      { path: 'all', component: ProductAllComponent}
    ]
  },
  { path: 'technology', component: TechnologyListComponent},
  { path: 'quality', component: QualityListComponent},
  { path: 'career', component: CareerListComponent},
  { path: 'jobs/:jobId', component: JobDetailsComponent},
  { path: 'jobs/:jobId/apply', component: ApplyComponent},
  { path: 'contact', component: ContactPageComponent},
  { path: 'technologiesused', component: TechnologiesUsedComponent},
  { path: '',
    redirectTo: '/en',
    pathMatch: 'full'
  },
  //{ path: "**", component: PageNotFoundComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [HomePageComponent, NewsListComponent, ProductsListComponent, TechnologyListComponent, QualityListComponent, CareerListComponent, ContactPageComponent, PageNotFoundComponent, ProductAtvComponent, ProductAutomobilePartsComponent, ProductExteriorTrimPartsComponent, ProductFunctionalPartsComponent, ProductMotorcycleComponent, ProductScooterMotorcycleComponent, ProductUnderboneComponent, ProductAllComponent, TechnologiesUsedComponent]

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
  public products =[];

  constructor(private _productService: ProductService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this._productService.getProducts()
      .subscribe(data => this.products = data);
  }

  showUnderbone() {
    this.router.navigate(['underbone'], {relativeTo: this.route});
  }

  showScooterMotorcycle() {
    this.router.navigate(['scooter-motorcycle'], {relativeTo: this.route});
  }

  showMotorcycle() {
    this.router.navigate(['motorcycle'], {relativeTo: this.route});
  }

  showAtv() {
    this.router.navigate(['atv'], {relativeTo: this.route});
  }

  showAutomobileParts() {
    this.router.navigate(['automobile-parts'], {relativeTo: this.route});
  }

  showFunctionalParts() {
    this.router.navigate(['functional-parts'], {relativeTo: this.route});
  }

  showExteriorTrimParts() {
    this.router.navigate(['exterior-trim-parts'], {relativeTo: this.route});
  }

  showAll() {
    this.router.navigate(['all'], {relativeTo: this.route});

  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Apply } from './resume';

@Injectable({
  providedIn: 'root'
})
export class ApplyService {

  _url = 'http://localhost:3000/sendresume';
  constructor(private _http: HttpClient) {}

  submit(user: Apply) {
    return this._http.post<any>(this._url, user);
  }

}

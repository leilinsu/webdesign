import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {jobs} from '../jobs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import 'firebase/storage';
import 'firebase/firestore';
import { Observable } from 'rxjs';
import { async } from '@angular/core/testing';


@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./apply.component.css']
})
export class ApplyComponent implements OnInit {

  public job;
  private resumeForm: AngularFirestoreCollection<any>;
  private doc: AngularFirestoreDocument<any>;
  applied = false;
  applying = false;
  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;
  uploaded = false;
  uploading = false;
  selectedFiles: FileList;
  applyForm: FormGroup;


  constructor(private route: ActivatedRoute, private fb:FormBuilder, private fs:AngularFirestore, private storage: AngularFireStorage) {
    this.resumeForm = this.fs.collection('resumes');
    this.doc = this.resumeForm.doc(`${Date.now()}`);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.job = jobs[+params.get('jobId')];
  });

    this.applyForm = this.fb.group({
      firstName:['', Validators.required],
      lastName:['', Validators.required],
      email:['', [Validators.required, Validators.email]],
      phone:['', [Validators.required, Validators.pattern('[0-9]*')]]
    })

  }

  uploadFile(event) {
    this.uploading = true;
    const file = event.target.files[0];
    console.log(file.name);
    const filePath = `resumes/${Date.now()}_${file.name}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    // observe percentage changes
    this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
      finalize(async() => {this.downloadURL = await fileRef.getDownloadURL().toPromise();
        this.resumeForm.add({url: this.downloadURL, filePath});
       })
   ).subscribe()
    this.uploaded = true;
    this.uploading = false;
  }

  onSubmit(value:any) {
    this.applying = true;
    this.doc.set(value);
    this.doc.update({url: this.downloadURL, job: this.job.name}).then(res => {
      this.applied = true;
    }).catch(err => console.log(err)).finally(() => {
      this.applying = false;
    });
    /*
    this.resumeForm.add(value).then(res => {
      this.applied = true;
    }).catch(err => console.log(err)).finally(() => {
      this.applying = false;
    });
    */
  }
}

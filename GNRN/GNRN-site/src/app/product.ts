export interface IProduct {
    name: string,
    category: string,
    product_code: string
}
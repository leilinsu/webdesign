import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductFunctionalPartsComponent } from './product-functional-parts.component';

describe('ProductFunctionalPartsComponent', () => {
  let component: ProductFunctionalPartsComponent;
  let fixture: ComponentFixture<ProductFunctionalPartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductFunctionalPartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductFunctionalPartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

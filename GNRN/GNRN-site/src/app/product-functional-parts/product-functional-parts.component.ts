import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-functional-parts',
  templateUrl: './product-functional-parts.component.html',
  styleUrls: ['./product-functional-parts.component.css']
})
export class ProductFunctionalPartsComponent implements OnInit {
  public products = [];

  constructor(private _productService: ProductService) { }

  ngOnInit() {
    this._productService.getProducts()
      .subscribe(data => this.products = data);
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'category'
})
export class CategoryPipe implements PipeTransform {

  transform(value: any[], cat: string): any {
    return value.filter((item) => item.category===cat);
  }

}

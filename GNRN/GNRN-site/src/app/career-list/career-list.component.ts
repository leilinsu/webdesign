import { Component, OnInit } from '@angular/core';
import { jobs } from'../jobs';


@Component({
  selector: 'app-career-list',
  templateUrl: './career-list.component.html',
  styleUrls: ['./career-list.component.css']
})
export class CareerListComponent implements OnInit {

  jobs = jobs;

  constructor() { }

  ngOnInit() {
  }

}

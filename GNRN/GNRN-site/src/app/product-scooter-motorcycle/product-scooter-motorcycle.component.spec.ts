import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductScooterMotorcycleComponent } from './product-scooter-motorcycle.component';

describe('ProductScooterMotorcycleComponent', () => {
  let component: ProductScooterMotorcycleComponent;
  let fixture: ComponentFixture<ProductScooterMotorcycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductScooterMotorcycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductScooterMotorcycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

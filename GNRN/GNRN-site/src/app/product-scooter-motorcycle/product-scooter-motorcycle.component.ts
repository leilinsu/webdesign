import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-scooter-motorcycle',
  templateUrl: './product-scooter-motorcycle.component.html',
  styleUrls: ['./product-scooter-motorcycle.component.css']
})
export class ProductScooterMotorcycleComponent implements OnInit {
  public products = [];

  constructor(private _productService: ProductService) { }

  ngOnInit() {
    this._productService.getProducts()
      .subscribe(data => this.products = data);
  }

}

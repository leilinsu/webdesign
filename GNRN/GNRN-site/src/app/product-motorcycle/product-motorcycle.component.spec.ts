import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductMotorcycleComponent } from './product-motorcycle.component';

describe('ProductMotorcycleComponent', () => {
  let component: ProductMotorcycleComponent;
  let fixture: ComponentFixture<ProductMotorcycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductMotorcycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductMotorcycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

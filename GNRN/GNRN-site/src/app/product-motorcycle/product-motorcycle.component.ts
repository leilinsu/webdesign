import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-motorcycle',
  templateUrl: './product-motorcycle.component.html',
  styleUrls: ['./product-motorcycle.component.css']
})
export class ProductMotorcycleComponent implements OnInit {
  public products = [];

  constructor(private _productService: ProductService) { }

  ngOnInit() {
    this._productService.getProducts()
      .subscribe(data => this.products = data);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductAutomobilePartsComponent } from './product-automobile-parts.component';

describe('ProductAutomobilePartsComponent', () => {
  let component: ProductAutomobilePartsComponent;
  let fixture: ComponentFixture<ProductAutomobilePartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductAutomobilePartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductAutomobilePartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

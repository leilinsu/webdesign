import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-automobile-parts',
  templateUrl: './product-automobile-parts.component.html',
  styleUrls: ['./product-automobile-parts.component.css']
})
export class ProductAutomobilePartsComponent implements OnInit {

  public products = [];

  constructor(private _productService: ProductService) { }

  ngOnInit() {
    this._productService.getProducts()
      .subscribe(data => this.products = data)
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import 'firebase/firestore';


@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.css']
})
export class ContactPageComponent implements OnInit {

  private submissionForm: AngularFirestoreCollection<any>;

  submitting = false;
  submitted = false;
  contactForm: FormGroup;

  constructor(private fb:FormBuilder, private fs:AngularFirestore) {
    this.submissionForm = this.fs.collection('submissions');
  }

  ngOnInit(){
    
      this.contactForm = this.fb.group({
      firstName:['', Validators.required],
      lastName:['', Validators.required],
      email:['', [Validators.required, Validators.email]],
      subject:['', Validators.required],
      message:['', Validators.required]
    })
    
  }

  onSubmit(value: any) {
    console.log(this.submitted);
    this.submitting = true;
    this.submissionForm.add(value).then(res => {
      this.submitted = true;
    }).catch(err => console.log(err)).finally(() => {
      this.submitting = false;
    });
  }
}
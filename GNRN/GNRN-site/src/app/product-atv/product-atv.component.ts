import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-atv',
  templateUrl: './product-atv.component.html',
  styleUrls: ['./product-atv.component.css']
})
export class ProductAtvComponent implements OnInit {
  
  public products = [];

  constructor(private _productService: ProductService) { }

  ngOnInit() {
    this._productService.getProducts()
      .subscribe(data => this.products = data)
  }

}

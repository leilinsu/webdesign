import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductAtvComponent } from './product-atv.component';

describe('ProductAtvComponent', () => {
  let component: ProductAtvComponent;
  let fixture: ComponentFixture<ProductAtvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductAtvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductAtvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, LOCALE_ID, Inject } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {

  languageList = [
    { code: 'en', label: 'English' },
    { code: 'zh-Hans', label: '简体中文' }
  ];

  constructor(private router: Router, @Inject(LOCALE_ID) protected localeId: string) {}

  ngOnInit() {
  }

  refresh(url: string, path: string) {
    this.router.navigateByUrl(url, { skipLocationChange: true }).then(() => {
      this.router.navigate([path]);
    }); 
  }
}
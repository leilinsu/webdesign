import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  /*
  @HostListener('window: scroll', ['$event'])
  onWindowScroll(e) {
    if (window.pageYOffset >= 25) {
      let element = document.getElementById('navBar');
      element.classList.add('sticky');
    }
    else {
      let element = document.getElementById('navBar');
      element.classList.remove('sticky');
    }
  }
  */
}

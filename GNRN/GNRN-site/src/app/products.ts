export const products = [
    {
        name: 'Carport Parts',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0015'
    },
    {
        name: 'Radio Holder',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0014'
    },
    {
        name: 'Side Door Panel',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0013'
    },
    {
        name: 'Rear Pillar Inner Decorative Assembly',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0012'
    },
    {
        name: 'Control Panel',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0011'
    },
    {
        name: 'Rear Door Trim Panel',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0010'
    },
    {
        name: 'Back Panel Trim Lower',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0009'
    },
    {
        name: 'Door Inside Handle',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0008'
    },
    {
        name: 'Steering Shroud',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0007'
    },
    {
        name: 'Seat Side-shield',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0006'
    },
    {
        name: 'Rear Door Trim Panel',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0005'
    },
    {
        name: 'Edge Trim Panel',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0004'
    },
    {
        name: 'Front&Rear Door&Window Trim Strips',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0003'
    },
    {
        name: 'Decorative Graning Parts',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0002'
    },
    {
        name: 'Package Tray Panel',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0001'
    },
    {
        name: 'Fender Fillet Series',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0027'
    },
    {
        name: 'Mud Flap Series',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0026'
    },
    {
        name: 'Radiator Air Deflector',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0025'
    },
    {
        name: 'Air Deflector',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0024'
    },
    {
        name: 'Spoiler Series 3',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0023'
    },
    {
        name: 'Spoiler Series 2',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0022'
    },
    {
        name: 'Spoiler Series 1',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0021'
    },
    {
        name: 'Side Skirt Series',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0020'
    },
    {
        name: 'Upper Grille',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0017'
    },
    {
        name: 'Ventilation Grille',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0016'
    },
    {
        name: 'Trunk Handle',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0018'
    },
    {
        name: 'Door Trim Parts',
        category: 'Exterior Trim Parts',
        product_code: 'GLA0019'
    },
    {
        name: 'Ventilation Duct',
        category: 'Functional Parts',
        product_code: 'GLA0036'
    },
    {
        name: 'Ashtray',
        category: 'Functional Parts',
        product_code: 'GLA0035'
    },
    {
        name: 'Air Conditioner Shell Series 3',
        category: 'Functional Parts',
        product_code: 'GLA0034'
    },
    {
        name: 'Air Conditioner Shell Series 2',
        category: 'Functional Parts',
        product_code: 'GLA0033'
    },
    {
        name: 'Air Conditioner Shell Series 1',
        category: 'Functional Parts',
        product_code: 'GLA0032'
    },
    {
        name: 'Battery Holder',
        category: 'Functional Parts',
        product_code: 'GLA0031'
    },
    {
        name: 'Overflow Tank Series',
        category: 'Functional Parts',
        product_code: 'GLA0030'
    },
    {
        name: 'Power Steering Pump Oil Pot Series',
        category: 'Functional Parts',
        product_code: 'GLA0029'
    },
    {
        name: 'Engine Cover Series',
        category: 'Functional Parts',
        product_code: 'GLA0028'
    },
    {
        name: 'Asian Tiger',
        category: 'Underbone',
        product_code: 'CLM0015'
    },
    {
        name: 'Asian Wind',
        category: 'Underbone',
        product_code: 'GLM0014'
    },
    {
        name: 'Asian Leopard',
        category: 'Underbone',
        product_code: 'GLM0013'
    },
    {
        name: 'Futuer Satr',
        category: 'Underbone',
        product_code: 'GLM0012'
    },
    {
        name: 'Promise Fox',
        category: 'Underbone',
        product_code: 'GLM0011'
    },
    {
        name: 'Thai Honda',
        category: 'Underbone',
        product_code: 'GLM0010'
    },
    {
        name: 'JS110-B',
        category: 'Underbone',
        product_code: 'GLM0009'
    },
    {
        name: 'Hongtu110',
        category: 'Underbone',
        product_code: 'GLM0008'
    },
    {
        name: 'Honda110',
        category: 'Underbone',
        product_code: 'GLM0007'
    },
    {
        name: '110-BF3',
        category: 'Underbone',
        product_code: 'GLM0005'
    },
    {
        name: '352',
        category: 'Underbone',
        product_code: 'GLM0006'
    },
    {
        name: '314',
        category: 'Motorcycle',
        product_code: 'GLM0001'
    },
    {
        name: 'TD125',
        category: 'Scooter Motorcycle',
        product_code: 'GLM0002'
    },
    {
        name: '400ATV',
        category: 'ATV',
        product_code: 'GLM0004'
    },
    {
        name: '250ATV',
        category: 'ATV',
        product_code: 'GLM0003'
    },
    {
        name: '3D Inner&Outer ribbed tube heater system',
        category: 'Automobile Parts',
        product_code: 'GLA0045'
    },
    {
        name: 'Expansion Tank',
        category: 'Automobile Parts',
        product_code: 'GLA0044'
    },
    {
        name: 'Front Wheel Rear Fender',
        category: 'Automobile Parts',
        product_code: 'GLA0043'
    },
    {
        name: 'Left&Right Side Door Trim Panel Armrests',
        category: 'Automobile Parts',
        product_code: 'GLA0042'
    },
    {
        name: 'Left&Right Side Gated Interior',
        category: 'Automobile Parts',
        product_code: 'GLA0041'
    },
    {
        name: 'Front Bumper Guard',
        category: 'Automobile Parts',
        product_code: 'GLA0040'
    },
    {
        name: 'Window Rain Guards',
        category: 'Automobile Parts',
        product_code: 'GLA0039'
    },
    {
        name: 'Tire Cover Trunk Board',
        category: 'Automobile Parts',
        product_code: 'GLA0038'
    },
    {
        name: 'Front Hole Cover',
        category: 'Automobile Parts',
        product_code: 'GLA0037'
    },
    {
        name: 'Air Filter Outlet Pipe',
        category: 'Automobile Parts',
        product_code: 'GLA0046'
    },
    {
        name: 'Intake Pipe',
        category: 'Automobile Parts',
        product_code: 'GLA0047'
    },
    {
        name: 'Intake Port Assembly',
        category: 'Automobile Parts',
        product_code: 'GLA0048'
    },
    {
        name: 'Air Outlet',
        category: 'Automobile Parts',
        product_code: 'GLA0049'
    },
    {
        name: 'Windshield Washer Tank',
        category: 'Automobile Parts',
        product_code: 'GLA0050'
    },
    {
        name: 'Gear Shift Control Panel',
        category: 'Automobile Parts',
        product_code: 'GLA0051'
    },
    {
        name: 'Lower Bumper Protection',
        category: 'Automobile Parts',
        product_code: 'GLA0052'
    },
    {
        name: 'Shift Knob Boot Dust Conver',
        category: 'Automobile Parts',
        product_code: 'GLA0053'
    }
];
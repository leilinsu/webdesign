import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-exterior-trim-parts',
  templateUrl: './product-exterior-trim-parts.component.html',
  styleUrls: ['./product-exterior-trim-parts.component.css']
})
export class ProductExteriorTrimPartsComponent implements OnInit {
  public products = [];

  constructor(private _productService: ProductService) { }

  ngOnInit() {
    this._productService.getProducts()
      .subscribe(data => this.products = data);
  }

}

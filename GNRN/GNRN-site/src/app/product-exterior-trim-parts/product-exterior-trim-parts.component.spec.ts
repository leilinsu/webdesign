import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductExteriorTrimPartsComponent } from './product-exterior-trim-parts.component';

describe('ProductExteriorTrimPartsComponent', () => {
  let component: ProductExteriorTrimPartsComponent;
  let fixture: ComponentFixture<ProductExteriorTrimPartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductExteriorTrimPartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductExteriorTrimPartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

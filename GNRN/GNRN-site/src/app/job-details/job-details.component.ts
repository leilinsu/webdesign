import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { jobs } from '../jobs';
@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.css']
})
export class JobDetailsComponent implements OnInit {

  public job;
  public resList:string[];
  public quaList: string[];

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.job = jobs[+params.get('jobId')];
    });

    this.resList = this.toList(this.job.responsibilities);
    this.quaList = this.toList(this.job.qualifications);
  }

  toList(paragraph: string): string[] {
    return paragraph.split("  ");
  }

  apply() {
    this.router.navigate(['apply'], {relativeTo: this.route});
  }


}

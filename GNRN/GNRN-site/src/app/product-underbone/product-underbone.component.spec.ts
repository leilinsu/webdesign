import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductUnderboneComponent } from './product-underbone.component';

describe('ProductUnderboneComponent', () => {
  let component: ProductUnderboneComponent;
  let fixture: ComponentFixture<ProductUnderboneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductUnderboneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductUnderboneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-underbone',
  templateUrl: './product-underbone.component.html',
  styleUrls: ['./product-underbone.component.css']
})
export class ProductUnderboneComponent implements OnInit {

  public products = [];

  constructor(private _productService: ProductService) { }

  ngOnInit() {
    this._productService.getProducts()
      .subscribe(data => this.products = data);
  }



}

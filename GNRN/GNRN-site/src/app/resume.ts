export class Apply {
    constructor(
        public job: string,
        public firstName: string,
        public lastName: string,
        public email: string,
        public phone: string
    ) {}
}